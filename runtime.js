const parser = require("./parser")
const fs = require('fs');
const primitiveTypes = require("./PrimitiveType");

let code = `
/*    calculates fibonacci sequence */
main () {
var a int = 10;
var b int = 5;
var c int = 2;
var d int = a / 2;
c = a;


if a != 10 {
    c = b;
} else {
    c = c + 16;
}

var i int = 0;


while i <= b {
    print : i;
    i = i + 1;
}

SetStorage('a', b);
var f int = GetStorage('a');
}

`

let ast = parser.parse(code);

console.log(ast);

let memory = {}
let storage = {}

let mainFunction = ast.mainFunction
let functionDefinitions = ast.functionDefinitions

console.log("function definitions");
for (const func of functionDefinitions) {
    console.log(func);
}

console.log("main function");
for (const statement of mainFunction.body.statements) {
    console.log(statement);
    executeStatement(statement)
}

console.log("memory", memory);
console.log("storage", storage);
// fs.writeFileSync("./ast.json", JSON.stringify(ast));

function executeStatement(statement){
    let kind = statement.kind
    switch (kind) {
        case "VariableDeclaration":

            let type = statement.type.kind;
            let name = statement.name.name;
            let initial = statement.initial;

            declareVariable(name, type, initial);
            
            break;
        case "ExpressionStatement":
            evaluateExpression(statement.expression)
            
            break;
        case "ConditionalStatement":
            evaluateConditionalStatement(statement)
            
            break;
        case "PrintStatement":
            evaluatePrintStatement(statement)
            
            break;
        case "LoopingStatement":
            evaluateLoopingStatement(statement)
            
            break;

        default:
            break;
    }
}

function setMemory(name, type, value){
    if(type === primitiveTypes.intType){
        memory[name] = value ? value : 0
    }
}

function getMemory(name, type="IntType"){
    let existingKeys = Object.keys(memory);
    if(!existingKeys.includes(name)){
        runningExeption("the variable '"+name+"' wasn't declared")
    } else {
        return {value: memory[name], type: type}
    }
}

function declareVariable(name, type, initial){
    if(type === primitiveTypes.intType){
        if(initial == null){
            setMemory(name, type, 0)
        } else if(initial.kind === "BinaryOperator") {
            setMemory(name, primitiveTypes.intType, evaluateSimpleBinaryOperator(initial));
        } else if(initial.kind === "FunctionApplication"){
            setMemory(name, type, executeFunction(initial))
        } else {
            setMemory(name, type, initial.value)
        }
    }
    runningExeption("error while creating variable type")
}


function evaluateSimpleBinaryOperator(binaryOperator) {
    // console.log(binaryOperator);
    if(binaryOperator.lhs === "BinaryOperator"){
        runningExeption("ddScript can't yet evaluate complex expressions")
    } else {
        let left = binaryOperator.lhs.kind === "Identifier" ? getMemory(binaryOperator.lhs.name).value : binaryOperator.lhs.value
        let right = binaryOperator.rhs.kind === "Identifier" ? getMemory(binaryOperator.rhs.name).value : binaryOperator.rhs.value
        // integers
        return eval(`${left} ${binaryOperator.operator} ${right}`)
    }
}

function getTokenVal(token){
    let value;
    if(token.kind === "Identifier"){
        value = getMemory(token.name).value
    } else if(token.kind === "BinaryOperator"){
        value = evaluateSimpleBinaryOperator(token)
    } else {
        value = token.value
    }
    return value;
}

function evaluateExpression(expression){
    if(expression.kind === "FunctionApplication"){
        executeFunction(expression);
    } else {
        if (expression.operator === "="){
            let right = getTokenVal(expression.rhs)
            // let right = expression.rhs.kind === "Identifier" ? getMemory(expression.rhs.name).value : expression.rhs.value
            setMemory(expression.lhs.name, primitiveTypes.intType, right)
            return
        } else {
            left = expression.lhs.kind === "Identifier" ? getMemory(expression.lhs.name).value : expression.lhs.value
            right = expression.rhs.kind === "Identifier" ? getMemory(expression.rhs.name).value : expression.rhs.value
            return eval(`${left} ${expression.operator} ${right}`)
        }
    }
}

function evaluateConditionalStatement(cs){
    if(evaluateExpression(cs.predicate)){
        let statements = cs.thenBody.statements;
        for (const statement of statements) {
            executeStatement(statement)
        }
    } else {
        let statements = cs.elseBody.statements;
        for (const statement of statements) {
            executeStatement(statement)
        }
    }
}

function evaluatePrintStatement(ps){
    if(ps.predicate.kind === "String"){
        console.log("LOG :: ", ps.predicate.string);
    } else if(ps.predicate.kind === "Identifier") {
        console.log("LOG :: ", getMemory(ps.predicate.name).value);
    } else if(ps.predicate.kind === "BinaryOperator") {
        console.log("LOG :: ", evaluateSimpleBinaryOperator(ps.predicate));
    } else {
        runningExeption("can't evaluate the print statement")
    }
}

function evaluateLoopingStatement(ls){
    if(ls.predicate.kind === "BinaryOperator"){
        if(typeof evaluateSimpleBinaryOperator(ls.predicate) === "boolean"){
            let condition = evaluateSimpleBinaryOperator(ls.predicate)
            while(condition){
                let statements = ls.doBody.statements;
                for (const statement of statements) {
                    executeStatement(statement)
                }
                condition = evaluateSimpleBinaryOperator(ls.predicate)
            }
        }
    }
}

function executeFunction(functionApplication){

    // let func = functionDefinitions.filter((value)=> value.name === name)[0];
    // let statements = func.body.statements;

    // for (const statement of statements) {
    //     executeStatement(statement);
    // }

    // native functions
    if(isNativeFunction(functionApplication.name)){
        switch (functionApplication.name) {
            case "GetStorage":
                // verify args
                verifyArgs(functionApplication, 1, ["String"])
                return getStorage(functionApplication.args[0].string)

            case "SetStorage":
                // verify args
                verifyArgs(functionApplication, 2, ["String", "Identifier"])
                setStorage(functionApplication.args[0].string, getTokenVal(functionApplication.args[1]))
                break;
        
            default:
                break;
        }
    }

    runningExeption("ddscript cannot yet evaluate functions")
}

function isNativeFunction(name){
    let nativeFunctions = ["GetStorage", "SetStorage"]

    if(nativeFunctions.find((val) => val === name)){
        return true
    }
    return false
}

function setStorage(key, value){
    storage[key] = value;
}

function getStorage(key){
    let existingKeys = Object.keys(storage);
    if(existingKeys.find((val) => val === key )){
        return storage[key]
    }

    return undefined
}

function verifyArgs(functionApplication, length=0, types=[]){
    if(functionApplication.args.length !== length){
        runningExeption(`Invalid number of arguments required ${length} but get ${functionApplication.args.length}`)
    }
    types.map((type, i) => {
        if(type !== functionApplication.args[i].kind){
            runningExeption(`Parameters dosen't match, type ${type} is not type ${functionApplication.args[i].kind}`)
        }
    })
}

function runningExeption(message){
    console.error("============= !!!! RUNNING EXCEPTION :: ", message);
    return
}